
//var socket;
//实现化WebSocket对象，指定要连接的服务器地址与端口
//var socket = new WebSocket("ws://192.168.1.171:8080/Socket_Demo1/ws/zhangsan");

var socket;

// 连接
function connect() {
	var user = document.getElementById("username").value;
	socket = new WebSocket("ws://192.168.1.171:8080/Socket_Demo1/ws/" + user);
	
	if(typeof(WebSocket) == "undefined") {
		alert("您的浏览器不支持WebSocket");
		return;
	}

	//打开事件
	socket.onopen = function() {
		alert("Socket 已打开");
		//socket.send("这是来自客户端的消息" + location.href + new Date());
	};
	//获得消息事件
	socket.onmessage = function(msg) {
		alert(msg.data);
	};

	//关闭事件
	socket.onclose = function() {
		alert("Socket已关闭");
	};
	
	//发生了错误事件
	socket.onerror = function() {
		alert("发生了错误");
	}
}

// 发送
function send() {
	var msg = document.getElementById("message").value;
	socket.send(msg);
}

// 关闭
function closeSocket() {
//	alert("草拟吗走没走这啊");
	socket.close();
}


